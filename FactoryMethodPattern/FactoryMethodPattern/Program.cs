﻿using System;

namespace FactoryMethodPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            MovieFactory movieFactory_comedy = new ComedyMovieFactory(new ComedyMovie());
            movieFactory_comedy.GetInformationAboutMovie();
            MovieFactory movieFactory_horror = new HorrorMovieFactory(new HorrorMovie());
            movieFactory_horror.GetInformationAboutMovie();
            MovieFactory movieFactory_thiller = new ThrillerMovieFactory(new ThrillerMovie());
            movieFactory_thiller.GetInformationAboutMovie();

            Console.ReadKey();
        }
    }

    public interface IMovieFactory
    {
        public string Title { get; }
        public int Year { get; }
        public string Type { get; }

        public void GetInformation();
    }

    public abstract class MovieFactory
    {
        private IMovieFactory context;

        public MovieFactory(IMovieFactory context)
        {
            this.context = context;
        }

        public void GetInformationAboutMovie()
        {
            this.context.GetInformation();
        }
    }

    public class ComedyMovieFactory : MovieFactory
    {
        public ComedyMovieFactory(IMovieFactory factory) : base(factory) { }
    }

    public class HorrorMovieFactory : MovieFactory
    {
        public HorrorMovieFactory(IMovieFactory factory) : base(factory) { }
    }

    public class ThrillerMovieFactory : MovieFactory
    {
        public ThrillerMovieFactory(IMovieFactory factory) : base(factory) { }
    }

    public class ComedyMovie : IMovieFactory
    {
        public string Title => "Scary Movie";

        public int Year => 2000;

        public string Type => "Comedy";

        public void GetInformation()
        {
            Console.WriteLine(
               this.Title + Environment.NewLine
               + this.Year.ToString() + Environment.NewLine
               + this.Type + Environment.NewLine);
        }
    }

    public class HorrorMovie : IMovieFactory
    {
        public string Title => "Wywiad z wampirem";

        public int Year => 1994;

        public string Type => "Horror";

        public void GetInformation()
        {
            Console.WriteLine(
               this.Title + Environment.NewLine
               + this.Year.ToString() + Environment.NewLine
               + this.Type + Environment.NewLine);
        }
    }

    public class ThrillerMovie : IMovieFactory
    {
        public string Title => "Joker";

        public int Year => 2019;

        public string Type => "Thriller";

        public void GetInformation()
        {
            Console.WriteLine(
               this.Title + Environment.NewLine
               + this.Year.ToString() + Environment.NewLine
               + this.Type + Environment.NewLine);
        }
    }
}
